

// Remaps a random value from between 0-1 to be within the specified min and max rang
float random_range(int ptnum; float min; float max){
    return fit(rand(ptnum), 0,1,min,max);
}


/// Creates a vector filled with random values between the specified min, max
vector random_vector(int ptnum; float min; float max){
    vector rnd = {0.0,0.0,0.0};

    rnd.x = random_range(ptnum,min,max);
    rnd.y = random_range(ptnum,min,max);
    rnd.z = random_range(ptnum,min,max);
    return rnd;
}


/// Creates a vector filled with random values between 0-1
vector random_vector(int ptnum){
    vector rnd = {0.0,0.0,0.0};

    rnd.x = random_range(ptnum,0,1);
    rnd.y = random_range(ptnum,0,1);
    rnd.z = random_range(ptnum,0,1);
    return rnd;
}

// generates a random position within a sphere. 
vector rand_pos_in_sphere(int radius; vector spherePosition; int onSurface){
    if(onSurface == true){
        return normalize(sample_sphere_uniform(nrandom())) * radius;
    }else{
        return spherePosition + sample_sphere_uniform(nrandom()) * radius;
    }
}

