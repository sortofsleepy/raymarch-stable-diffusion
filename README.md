# Raymarch Stable Diffusion

An experimental Houdini project to integrate Stable Diffusion(via HuggingFace) with Houdini COPs 


Setup
====
- First open up the `particle_sim_idea` file to generate the file cache necessary to create the main scene to input into Stable Diffusion. 
  - Optionally, add your own scene and make the necessary connections accordingly or copy/paste the nodes into the `raymarch-stable-diff` project file. 
- Next, make sure you have the necessary Python packages to interface with Stable Diffusion and HuggingFace's Diffusers library. The interface on the Houdini side is set up as a Digital Asset file. See [here](https://gitlab.com/xoio/stable-diffusion-hda) for how to set up and additional notes about how the Stable Diffusion integration works. 
- Note this has only been tested on Windows and an NVidia GPU. 


Running
===
- The main thing to manipulate is an Image Network. You can find it under the `img` section of the node explorer window. 
- Note that the Stable Diffusion node will initially be set to be bypassed(yellow toggle on the node), this is to help mitigate having to wait on a new frame with each change you make as each frame averages about a minute to generate for a 1024x1024 image on a 3080. 
- You'll know things are running once you see the progress indicator(s) pop up in the Houdini console. 


Final Notes
===
The setup is largely based on the following Entagma tutorials 

- [https://entagma.com/diy-rendering-engine-like-its-1975/](https://entagma.com/diy-rendering-engine-like-its-1975/)
- [https://entagma.com/abstract-particle-animation-using-houdini-vellum-octane-render/](https://entagma.com/abstract-particle-animation-using-houdini-vellum-octane-render/)

as well as other resources which are noted. Many thanks to all of these fine people for sharing their knowledge!